﻿namespace RPG_Character_Generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.CharacterTextBox = new System.Windows.Forms.TextBox();
            this.Thief = new System.Windows.Forms.RadioButton();
            this.Wizard = new System.Windows.Forms.RadioButton();
            this.Healer = new System.Windows.Forms.RadioButton();
            this.Warrior = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AttackInput = new System.Windows.Forms.NumericUpDown();
            this.AttackLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.MagicPanel = new System.Windows.Forms.Panel();
            this.MagicInput = new System.Windows.Forms.NumericUpDown();
            this.MagicLabel = new System.Windows.Forms.Label();
            this.DefenceInput = new System.Windows.Forms.NumericUpDown();
            this.DefenceLabel = new System.Windows.Forms.Label();
            this.SpeedInput = new System.Windows.Forms.NumericUpDown();
            this.SpeedLabel = new System.Windows.Forms.Label();
            this.ManaInput = new System.Windows.Forms.NumericUpDown();
            this.ManaLabel = new System.Windows.Forms.Label();
            this.HealingPanel = new System.Windows.Forms.Panel();
            this.HealingInput = new System.Windows.Forms.NumericUpDown();
            this.Healing = new System.Windows.Forms.Label();
            this.HamPanel = new System.Windows.Forms.Panel();
            this.HamCheckBox = new System.Windows.Forms.CheckBox();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.MagicAttackPanel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttackInput)).BeginInit();
            this.panel2.SuspendLayout();
            this.MagicPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MagicInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefenceInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManaInput)).BeginInit();
            this.HealingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HealingInput)).BeginInit();
            this.HamPanel.SuspendLayout();
            this.MagicAttackPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(50, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // CharacterTextBox
            // 
            this.CharacterTextBox.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CharacterTextBox.ForeColor = System.Drawing.Color.Silver;
            this.CharacterTextBox.Location = new System.Drawing.Point(135, 41);
            this.CharacterTextBox.Name = "CharacterTextBox";
            this.CharacterTextBox.Size = new System.Drawing.Size(648, 32);
            this.CharacterTextBox.TabIndex = 1;
            this.CharacterTextBox.Text = "Enter character name...";
            this.CharacterTextBox.TextChanged += new System.EventHandler(this.CharacterTextBox_TextChanged);
            this.CharacterTextBox.Enter += new System.EventHandler(this.CharacterTextBox_Enter);
            this.CharacterTextBox.Leave += new System.EventHandler(this.CharacterTextBox_Leave);
            // 
            // Thief
            // 
            this.Thief.AutoSize = true;
            this.Thief.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Thief.Location = new System.Drawing.Point(15, 14);
            this.Thief.Name = "Thief";
            this.Thief.Size = new System.Drawing.Size(78, 28);
            this.Thief.TabIndex = 3;
            this.Thief.TabStop = true;
            this.Thief.Text = "Thief";
            this.Thief.UseVisualStyleBackColor = true;
            // 
            // Wizard
            // 
            this.Wizard.AutoSize = true;
            this.Wizard.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Wizard.Location = new System.Drawing.Point(209, 14);
            this.Wizard.Name = "Wizard";
            this.Wizard.Size = new System.Drawing.Size(94, 28);
            this.Wizard.TabIndex = 4;
            this.Wizard.TabStop = true;
            this.Wizard.Text = "Wizard";
            this.Wizard.UseVisualStyleBackColor = true;
            this.Wizard.CheckedChanged += new System.EventHandler(this.Wizard_CheckedChanged);
            // 
            // Healer
            // 
            this.Healer.AutoSize = true;
            this.Healer.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Healer.Location = new System.Drawing.Point(648, 14);
            this.Healer.Name = "Healer";
            this.Healer.Size = new System.Drawing.Size(88, 28);
            this.Healer.TabIndex = 5;
            this.Healer.TabStop = true;
            this.Healer.Text = "Healer";
            this.Healer.UseVisualStyleBackColor = true;
            this.Healer.CheckedChanged += new System.EventHandler(this.Healer_CheckedChanged);
            // 
            // Warrior
            // 
            this.Warrior.AutoSize = true;
            this.Warrior.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Warrior.Location = new System.Drawing.Point(413, 14);
            this.Warrior.Name = "Warrior";
            this.Warrior.Size = new System.Drawing.Size(103, 28);
            this.Warrior.TabIndex = 6;
            this.Warrior.TabStop = true;
            this.Warrior.Text = "Warrior";
            this.Warrior.UseVisualStyleBackColor = true;
            this.Warrior.CheckedChanged += new System.EventHandler(this.Warrior_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Healer);
            this.panel1.Controls.Add(this.Warrior);
            this.panel1.Controls.Add(this.Thief);
            this.panel1.Controls.Add(this.Wizard);
            this.panel1.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(47, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(741, 52);
            this.panel1.TabIndex = 3;
            // 
            // AttackInput
            // 
            this.AttackInput.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttackInput.Location = new System.Drawing.Point(74, 25);
            this.AttackInput.Name = "AttackInput";
            this.AttackInput.Size = new System.Drawing.Size(86, 32);
            this.AttackInput.TabIndex = 6;
            // 
            // AttackLabel
            // 
            this.AttackLabel.AutoSize = true;
            this.AttackLabel.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttackLabel.Location = new System.Drawing.Point(3, 27);
            this.AttackLabel.Name = "AttackLabel";
            this.AttackLabel.Size = new System.Drawing.Size(61, 24);
            this.AttackLabel.TabIndex = 7;
            this.AttackLabel.Text = "Attack";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.SpeedInput);
            this.panel2.Controls.Add(this.SpeedLabel);
            this.panel2.Controls.Add(this.DefenceInput);
            this.panel2.Controls.Add(this.DefenceLabel);
            this.panel2.Controls.Add(this.AttackInput);
            this.panel2.Controls.Add(this.AttackLabel);
            this.panel2.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(47, 167);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 187);
            this.panel2.TabIndex = 8;
            // 
            // MagicPanel
            // 
            this.MagicPanel.Controls.Add(this.MagicAttackPanel);
            this.MagicPanel.Controls.Add(this.HealingPanel);
            this.MagicPanel.Controls.Add(this.ManaInput);
            this.MagicPanel.Controls.Add(this.ManaLabel);
            this.MagicPanel.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MagicPanel.Location = new System.Drawing.Point(256, 167);
            this.MagicPanel.Name = "MagicPanel";
            this.MagicPanel.Size = new System.Drawing.Size(246, 187);
            this.MagicPanel.TabIndex = 9;
            this.MagicPanel.Visible = false;
            // 
            // MagicInput
            // 
            this.MagicInput.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MagicInput.Location = new System.Drawing.Point(133, 5);
            this.MagicInput.Name = "MagicInput";
            this.MagicInput.Size = new System.Drawing.Size(86, 32);
            this.MagicInput.TabIndex = 6;
            // 
            // MagicLabel
            // 
            this.MagicLabel.AutoSize = true;
            this.MagicLabel.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MagicLabel.Location = new System.Drawing.Point(8, 11);
            this.MagicLabel.Name = "MagicLabel";
            this.MagicLabel.Size = new System.Drawing.Size(117, 24);
            this.MagicLabel.TabIndex = 7;
            this.MagicLabel.Text = "Magic Attack";
            // 
            // DefenceInput
            // 
            this.DefenceInput.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefenceInput.Location = new System.Drawing.Point(74, 63);
            this.DefenceInput.Name = "DefenceInput";
            this.DefenceInput.Size = new System.Drawing.Size(86, 32);
            this.DefenceInput.TabIndex = 8;
            // 
            // DefenceLabel
            // 
            this.DefenceLabel.AutoSize = true;
            this.DefenceLabel.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefenceLabel.Location = new System.Drawing.Point(3, 65);
            this.DefenceLabel.Name = "DefenceLabel";
            this.DefenceLabel.Size = new System.Drawing.Size(69, 24);
            this.DefenceLabel.TabIndex = 9;
            this.DefenceLabel.Text = "Defence";
            // 
            // SpeedInput
            // 
            this.SpeedInput.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpeedInput.Location = new System.Drawing.Point(74, 101);
            this.SpeedInput.Name = "SpeedInput";
            this.SpeedInput.Size = new System.Drawing.Size(86, 32);
            this.SpeedInput.TabIndex = 10;
            // 
            // SpeedLabel
            // 
            this.SpeedLabel.AutoSize = true;
            this.SpeedLabel.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpeedLabel.Location = new System.Drawing.Point(3, 103);
            this.SpeedLabel.Name = "SpeedLabel";
            this.SpeedLabel.Size = new System.Drawing.Size(59, 24);
            this.SpeedLabel.TabIndex = 11;
            this.SpeedLabel.Text = "Speed";
            // 
            // ManaInput
            // 
            this.ManaInput.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManaInput.Location = new System.Drawing.Point(136, 25);
            this.ManaInput.Name = "ManaInput";
            this.ManaInput.Size = new System.Drawing.Size(86, 32);
            this.ManaInput.TabIndex = 8;
            // 
            // ManaLabel
            // 
            this.ManaLabel.AutoSize = true;
            this.ManaLabel.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManaLabel.Location = new System.Drawing.Point(11, 27);
            this.ManaLabel.Name = "ManaLabel";
            this.ManaLabel.Size = new System.Drawing.Size(58, 24);
            this.ManaLabel.TabIndex = 9;
            this.ManaLabel.Text = "Mana";
            // 
            // HealingPanel
            // 
            this.HealingPanel.Controls.Add(this.HealingInput);
            this.HealingPanel.Controls.Add(this.Healing);
            this.HealingPanel.Location = new System.Drawing.Point(3, 97);
            this.HealingPanel.Name = "HealingPanel";
            this.HealingPanel.Size = new System.Drawing.Size(240, 53);
            this.HealingPanel.TabIndex = 10;
            this.HealingPanel.Visible = false;
            // 
            // HealingInput
            // 
            this.HealingInput.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HealingInput.Location = new System.Drawing.Point(133, 6);
            this.HealingInput.Name = "HealingInput";
            this.HealingInput.Size = new System.Drawing.Size(86, 32);
            this.HealingInput.TabIndex = 10;
            // 
            // Healing
            // 
            this.Healing.AutoSize = true;
            this.Healing.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Healing.Location = new System.Drawing.Point(8, 8);
            this.Healing.Name = "Healing";
            this.Healing.Size = new System.Drawing.Size(85, 29);
            this.Healing.TabIndex = 11;
            this.Healing.Text = "Healing";
            // 
            // HamPanel
            // 
            this.HamPanel.Controls.Add(this.HamCheckBox);
            this.HamPanel.Font = new System.Drawing.Font("Wide Latin", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HamPanel.Location = new System.Drawing.Point(543, 205);
            this.HamPanel.Name = "HamPanel";
            this.HamPanel.Size = new System.Drawing.Size(200, 100);
            this.HamPanel.TabIndex = 10;
            this.HamPanel.Visible = false;
            // 
            // HamCheckBox
            // 
            this.HamCheckBox.AutoSize = true;
            this.HamCheckBox.Location = new System.Drawing.Point(47, 33);
            this.HamCheckBox.Name = "HamCheckBox";
            this.HamCheckBox.Size = new System.Drawing.Size(122, 28);
            this.HamCheckBox.TabIndex = 11;
            this.HamCheckBox.Text = "Ham";
            this.HamCheckBox.UseVisualStyleBackColor = true;
            // 
            // SubmitButton
            // 
            this.SubmitButton.Font = new System.Drawing.Font("Ravie", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitButton.Location = new System.Drawing.Point(280, 385);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(241, 53);
            this.SubmitButton.TabIndex = 11;
            this.SubmitButton.Text = "Create Character";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // MagicAttackPanel
            // 
            this.MagicAttackPanel.Controls.Add(this.MagicLabel);
            this.MagicAttackPanel.Controls.Add(this.MagicInput);
            this.MagicAttackPanel.Location = new System.Drawing.Point(3, 54);
            this.MagicAttackPanel.Name = "MagicAttackPanel";
            this.MagicAttackPanel.Size = new System.Drawing.Size(240, 41);
            this.MagicAttackPanel.TabIndex = 11;
            this.MagicAttackPanel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 484);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.HamPanel);
            this.Controls.Add(this.MagicPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CharacterTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Create an RPG character";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttackInput)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.MagicPanel.ResumeLayout(false);
            this.MagicPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MagicInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefenceInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManaInput)).EndInit();
            this.HealingPanel.ResumeLayout(false);
            this.HealingPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HealingInput)).EndInit();
            this.HamPanel.ResumeLayout(false);
            this.HamPanel.PerformLayout();
            this.MagicAttackPanel.ResumeLayout(false);
            this.MagicAttackPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CharacterTextBox;
        private System.Windows.Forms.RadioButton Warrior;
        private System.Windows.Forms.RadioButton Healer;
        private System.Windows.Forms.RadioButton Wizard;
        private System.Windows.Forms.RadioButton Thief;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown AttackInput;
        private System.Windows.Forms.Label AttackLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown SpeedInput;
        private System.Windows.Forms.Label SpeedLabel;
        private System.Windows.Forms.NumericUpDown DefenceInput;
        private System.Windows.Forms.Label DefenceLabel;
        private System.Windows.Forms.Panel MagicPanel;
        private System.Windows.Forms.NumericUpDown MagicInput;
        private System.Windows.Forms.Label MagicLabel;
        private System.Windows.Forms.NumericUpDown ManaInput;
        private System.Windows.Forms.Label ManaLabel;
        private System.Windows.Forms.Panel HealingPanel;
        private System.Windows.Forms.NumericUpDown HealingInput;
        private System.Windows.Forms.Label Healing;
        private System.Windows.Forms.Panel HamPanel;
        private System.Windows.Forms.CheckBox HamCheckBox;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.Panel MagicAttackPanel;
    }
}

