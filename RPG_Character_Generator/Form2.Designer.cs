﻿namespace RPG_Character_Generator
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.Header = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.Attack = new System.Windows.Forms.Label();
            this.Defence = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TextBoxLabel = new System.Windows.Forms.Label();
            this.HpValue = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.NameValue = new System.Windows.Forms.Label();
            this.MagicPanel = new System.Windows.Forms.Panel();
            this.Magic = new System.Windows.Forms.Label();
            this.MagicValue = new System.Windows.Forms.Label();
            this.Hp = new System.Windows.Forms.Label();
            this.SpeedValue = new System.Windows.Forms.Label();
            this.Speed = new System.Windows.Forms.Label();
            this.AttackValue = new System.Windows.Forms.Label();
            this.DefenceValue = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.MagicPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.AutoSize = true;
            this.Header.BackColor = System.Drawing.SystemColors.Control;
            this.Header.Font = new System.Drawing.Font("Old English Text MT", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Header.Location = new System.Drawing.Point(3, 13);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(104, 33);
            this.Header.TabIndex = 0;
            this.Header.Text = "Header";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLabel.Location = new System.Drawing.Point(5, 57);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(71, 24);
            this.NameLabel.TabIndex = 1;
            this.NameLabel.Text = "Name:";
            // 
            // Attack
            // 
            this.Attack.AutoSize = true;
            this.Attack.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Attack.Location = new System.Drawing.Point(5, 119);
            this.Attack.Name = "Attack";
            this.Attack.Size = new System.Drawing.Size(126, 24);
            this.Attack.TabIndex = 3;
            this.Attack.Text = "Attack Power:";
            // 
            // Defence
            // 
            this.Defence.AutoSize = true;
            this.Defence.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Defence.Location = new System.Drawing.Point(5, 143);
            this.Defence.Name = "Defence";
            this.Defence.Size = new System.Drawing.Size(75, 24);
            this.Defence.TabIndex = 6;
            this.Defence.Text = "Defence:";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.TextBoxLabel);
            this.panel1.Controls.Add(this.HpValue);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.NameValue);
            this.panel1.Controls.Add(this.MagicPanel);
            this.panel1.Controls.Add(this.Hp);
            this.panel1.Controls.Add(this.SpeedValue);
            this.panel1.Controls.Add(this.Speed);
            this.panel1.Controls.Add(this.AttackValue);
            this.panel1.Controls.Add(this.DefenceValue);
            this.panel1.Controls.Add(this.Header);
            this.panel1.Controls.Add(this.NameLabel);
            this.panel1.Controls.Add(this.Defence);
            this.panel1.Controls.Add(this.Attack);
            this.panel1.Location = new System.Drawing.Point(12, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(226, 375);
            this.panel1.TabIndex = 8;
            // 
            // TextBoxLabel
            // 
            this.TextBoxLabel.AutoSize = true;
            this.TextBoxLabel.Location = new System.Drawing.Point(6, 346);
            this.TextBoxLabel.Name = "TextBoxLabel";
            this.TextBoxLabel.Size = new System.Drawing.Size(78, 20);
            this.TextBoxLabel.TabIndex = 0;
            this.TextBoxLabel.Text = "Filename:";
            this.TextBoxLabel.Visible = false;
            // 
            // HpValue
            // 
            this.HpValue.AutoSize = true;
            this.HpValue.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HpValue.Location = new System.Drawing.Point(148, 167);
            this.HpValue.Name = "HpValue";
            this.HpValue.Size = new System.Drawing.Size(20, 24);
            this.HpValue.TabIndex = 16;
            this.HpValue.Text = "0";
            // 
            // textBox1
            // 
            this.textBox1.ForeColor = System.Drawing.Color.Silver;
            this.textBox1.Location = new System.Drawing.Point(90, 346);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(129, 26);
            this.textBox1.TabIndex = 9;
            this.textBox1.Text = "Enter filename...";
            this.textBox1.Visible = false;
            this.textBox1.Enter += new System.EventHandler(this.textBox1_Enter);
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // NameValue
            // 
            this.NameValue.AutoSize = true;
            this.NameValue.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameValue.Location = new System.Drawing.Point(147, 57);
            this.NameValue.Name = "NameValue";
            this.NameValue.Size = new System.Drawing.Size(20, 24);
            this.NameValue.TabIndex = 14;
            this.NameValue.Text = "0";
            // 
            // MagicPanel
            // 
            this.MagicPanel.AutoSize = true;
            this.MagicPanel.Controls.Add(this.Magic);
            this.MagicPanel.Controls.Add(this.MagicValue);
            this.MagicPanel.Location = new System.Drawing.Point(3, 206);
            this.MagicPanel.Name = "MagicPanel";
            this.MagicPanel.Size = new System.Drawing.Size(193, 94);
            this.MagicPanel.TabIndex = 9;
            this.MagicPanel.Visible = false;
            // 
            // Magic
            // 
            this.Magic.AutoSize = true;
            this.Magic.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Magic.Location = new System.Drawing.Point(3, 16);
            this.Magic.Name = "Magic";
            this.Magic.Size = new System.Drawing.Size(139, 24);
            this.Magic.TabIndex = 14;
            this.Magic.Text = "Magic/Healing:";
            // 
            // MagicValue
            // 
            this.MagicValue.AutoSize = true;
            this.MagicValue.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MagicValue.Location = new System.Drawing.Point(145, 16);
            this.MagicValue.Name = "MagicValue";
            this.MagicValue.Size = new System.Drawing.Size(20, 24);
            this.MagicValue.TabIndex = 15;
            this.MagicValue.Text = "0";
            // 
            // Hp
            // 
            this.Hp.AutoSize = true;
            this.Hp.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Hp.Location = new System.Drawing.Point(6, 167);
            this.Hp.Name = "Hp";
            this.Hp.Size = new System.Drawing.Size(50, 24);
            this.Hp.TabIndex = 15;
            this.Hp.Text = "HP:";
            // 
            // SpeedValue
            // 
            this.SpeedValue.AutoSize = true;
            this.SpeedValue.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpeedValue.Location = new System.Drawing.Point(147, 95);
            this.SpeedValue.Name = "SpeedValue";
            this.SpeedValue.Size = new System.Drawing.Size(20, 24);
            this.SpeedValue.TabIndex = 13;
            this.SpeedValue.Text = "0";
            // 
            // Speed
            // 
            this.Speed.AutoSize = true;
            this.Speed.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Speed.Location = new System.Drawing.Point(5, 95);
            this.Speed.Name = "Speed";
            this.Speed.Size = new System.Drawing.Size(65, 24);
            this.Speed.TabIndex = 12;
            this.Speed.Text = "Speed:";
            // 
            // AttackValue
            // 
            this.AttackValue.AutoSize = true;
            this.AttackValue.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttackValue.Location = new System.Drawing.Point(147, 119);
            this.AttackValue.Name = "AttackValue";
            this.AttackValue.Size = new System.Drawing.Size(20, 24);
            this.AttackValue.TabIndex = 9;
            this.AttackValue.Text = "0";
            // 
            // DefenceValue
            // 
            this.DefenceValue.AutoSize = true;
            this.DefenceValue.Font = new System.Drawing.Font("Old English Text MT", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefenceValue.Location = new System.Drawing.Point(147, 143);
            this.DefenceValue.Name = "DefenceValue";
            this.DefenceValue.Size = new System.Drawing.Size(20, 24);
            this.DefenceValue.TabIndex = 8;
            this.DefenceValue.Text = "0";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Ravie", 9F);
            this.button1.Location = new System.Drawing.Point(12, 461);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(226, 39);
            this.button1.TabIndex = 9;
            this.button1.Text = "Save Character";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(635, 602);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Name = "Form2";
            this.Text = "Character Details";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.MagicPanel.ResumeLayout(false);
            this.MagicPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Header;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label Attack;
        private System.Windows.Forms.Label Defence;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label AttackValue;
        private System.Windows.Forms.Label DefenceValue;
        private System.Windows.Forms.Label MagicValue;
        private System.Windows.Forms.Label Magic;
        private System.Windows.Forms.Label SpeedValue;
        private System.Windows.Forms.Label Speed;
        private System.Windows.Forms.Panel MagicPanel;
        private System.Windows.Forms.Label NameValue;
        private System.Windows.Forms.Label HpValue;
        private System.Windows.Forms.Label Hp;
        private System.Windows.Forms.Label TextBoxLabel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
    }
}