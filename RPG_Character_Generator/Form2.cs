﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Heroes;
using Microsoft.Data.SqlClient;
//using Newtonsoft.Json;

namespace RPG_Character_Generator
{
    public partial class Form2 : Form
    {
        private string SubCharType { get; set; }
        private Hero Character { get; set; }
        public Form2(Hero hero)
        {
            Character = hero;
            Character.Name = "Hamish";
            SubCharType = null;
            InitializeComponent();
            if(hero.IsHam)
            {
                FontFamily fontFamily = new FontFamily("Showcard Gothic");
                Font font = new Font(
                   fontFamily,
                   14,
                   FontStyle.Bold,
                   GraphicsUnit.Pixel);
                Header.Font = font;
                Header.Text = "This is Ham, A Bad MTFer";
                NameValue.Text = Character.Name;
                MagicPanel.Visible = true;
                Magic.Text = "Magic Attack";
                MagicValue.Text = "100";
            }
            else
            {
                Header.Text = $"{hero.Name} the Hobbit";
                NameValue.Text = hero.Name;
            }
            AttackValue.Text = hero.Attack.ToString();
            SpeedValue.Text = hero.SpeedMpS.ToString();
            DefenceValue.Text = hero.Defense.ToString();
            HpValue.Text = hero.Hp.ToString();
        }
        public Form2(Warrior hero)
        {
            Character = hero;
            SubCharType = "Warrior";
            InitializeComponent();

            Header.Text = $"{hero.Name} the Warrior";
            NameValue.Text = hero.Name;
            SpeedValue.Text = hero.SpeedMpS.ToString();
            DefenceValue.Text = hero.Defense.ToString();
            AttackValue.Text = hero.Attack.ToString();
            HpValue.Text = hero.Hp.ToString();
        }
        public Form2(Thief hero)
        {
            Character = hero;
            SubCharType = "Thief";
            InitializeComponent();

            Header.Text = $"{hero.Name} the Thief";
            NameValue.Text = hero.Name;
            SpeedValue.Text = hero.SpeedMpS.ToString();
            DefenceValue.Text = hero.Defense.ToString();
            AttackValue.Text = hero.Attack.ToString();
            HpValue.Text = hero.Hp.ToString();
        }
        public Form2(Wizard hero)
        {
            Character = hero;
            SubCharType = "Wizard";
            InitializeComponent();

            Header.Text = $"{hero.Name} the Wizard";
            NameValue.Text = hero.Name;
            SpeedValue.Text = hero.SpeedMpS.ToString();
            DefenceValue.Text = hero.Defense.ToString();
            AttackValue.Text = hero.Attack.ToString();
            HpValue.Text = hero.Hp.ToString();

            MagicPanel.Visible = true;
            Magic.Text = "Magic Attack";
            MagicValue.Text = hero.MagicAttack.ToString();            
        }
        public Form2(Healer hero)
        {
            Character = hero;
            SubCharType = "Healer";
            InitializeComponent();

            Header.Text = $"{hero.Name} the Healer";
            NameValue.Text = hero.Name;
            SpeedValue.Text = hero.SpeedMpS.ToString();
            DefenceValue.Text = hero.Defense.ToString();
            AttackValue.Text = hero.Attack.ToString();
            HpValue.Text = hero.Hp.ToString();

            MagicPanel.Visible = true;
            Magic.Text = "Magic Ability";
            MagicValue.Text = "Healing";
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.ForeColor = Color.LightGray;
                textBox1.Text = "Enter filename...";
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Enter filename...")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
            }
        }

        /** Save character to the local RPG_Characters Database folder on button click **/
        private void button1_Click(object sender, EventArgs e)
        {

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"localhost\SQLEXPRESS01";
            builder.InitialCatalog = "RPG_Characters";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql;

                    /**
                     * Create SQL query-string. 
                     * Checking if the character is a sub-character or just a base hero type.
                     */

                    if (SubCharType != null)
                    {
                        string getSql = $"SELECT ID FROM SubClass WHERE Name='{SubCharType}'";                            
                        sql = $"INSERT INTO BaseClass (Name,Speed,Attack,Defence,HP,SubClassID) VALUES (@Name,@Speed,@Attack,@Defence,@HP, ({getSql}))";
                    }
                    else
                    {
                       sql = "INSERT INTO BaseClass (Name,Speed,Attack,Defence,HP) VALUES (@Name,@Speed,@Attack,@Defence,@HP)";
                    }
                                       
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {                       
                        command.Parameters.AddWithValue("@Name", Character.Name);
                        command.Parameters.AddWithValue("@Speed", Character.SpeedMpS);
                        command.Parameters.AddWithValue("@Attack", Character.Attack);
                        command.Parameters.AddWithValue("@Defence", Character.Defense);
                        command.Parameters.AddWithValue("@HP", Character.Hp);
                        command.ExecuteNonQuery();
                    }
                }

                MessageBox.Show("Added character to database");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);

            }

            /** Below follows the version writing character info to textfile:
             * 
             * This version saves the character info as a JSON textfile 
             * to the MyDocuments folder on button click
             * 

            string filename;
            bool fileExists;
            string currentFilePath;
            int counter = 1;
            // Get path to documents folder
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (textBox1.Text == "" || textBox1.Text == "Enter filename...")
            {
                filename = "rpg-character";
            }
            else
            {
                filename = textBox1.Text;
            }

            // Check if filename is not in use, and create a new filename if so.
            do
            {
                currentFilePath = documentsPath + @"\" + filename + ".txt";
                fileExists = File.Exists(currentFilePath);
                filename = filename + counter.ToString();
                counter++;

            }
            while (fileExists);

            // Save to file 
            using (StreamWriter file = File.CreateText(currentFilePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, Character);
            }
            */
        }
    }
}