﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Heroes;

namespace RPG_Character_Generator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void SubmitButton_Click(object sender, EventArgs e)
        {
            float attack = Convert.ToSingle(AttackInput.Value);
            float defence = Convert.ToSingle(DefenceInput.Value);
            float speed = Convert.ToSingle(SpeedInput.Value);
            int mana = Convert.ToInt32(MagicInput.Value);
            float magicAttack = Convert.ToSingle(MagicInput.Value);
            float healing = Convert.ToSingle(HealingInput.Value);
            string name;

            if (CharacterTextBox.Text == "" || CharacterTextBox.Text == "Enter character name...")
            {
                name = "Unknown hero";
            }
            else
            {
                name = CharacterTextBox.Text;
            }

            if (HamCheckBox.Checked && CharacterTextBox.Text == "Bad Mother Fucker")
            {
                Hero hero = new Hero(defence, speed, true);
                Form2 newForm = new Form2(hero);
                newForm.Show();
            }
            else
            {
                if (Thief.Checked)
                {
                    Thief hero = new Thief(attack, defence, speed, name);
                    Form2 newForm = new Form2(hero);
                    newForm.Show();
                }
                else if (Warrior.Checked)
                {
                    Warrior hero = new Warrior(attack, defence, speed, name);
                    Form2 newForm = new Form2(hero);
                    newForm.Show();
                }
                else if (Wizard.Checked)
                {
                    Wizard hero = new Wizard(attack, magicAttack, mana, defence, speed, name);
                    Form2 newForm = new Form2(hero);
                    newForm.Show();
                }
                else if (Healer.Checked)
                {
                    Healer hero = new Healer(attack, defence, speed, name, healing, mana);
                    Form2 newForm = new Form2(hero);
                    newForm.Show();
                }
                else
                {
                    Hero hero = new Hero();
                    Form2 newForm = new Form2(hero);
                    newForm.Show();
                }
            }  
            

        }

        private void CharacterTextBox_TextChanged(object sender, EventArgs e)
        {
            if(CharacterTextBox.Text == "Bad Mother Fucker")
            {
                HamPanel.Visible = true;
            }
            else
            {
                HamPanel.Visible = false;
                HamCheckBox.Checked = false;
            }
        }

        private void CharacterTextBox_Leave(object sender, EventArgs e)
        {
            if (CharacterTextBox.Text == "")
            {
                CharacterTextBox.ForeColor = Color.LightGray;
                CharacterTextBox.Text = "Enter character name...";
            }
        }

        private void CharacterTextBox_Enter(object sender, EventArgs e)
        {
            if (CharacterTextBox.Text == "Enter character name...")
            {
                CharacterTextBox.Text = "";
                CharacterTextBox.ForeColor = Color.Black;
            }
        }

        private void Warrior_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Healer_CheckedChanged(object sender, EventArgs e)
        {
            if(Healer.Checked)
            {
                MagicPanel.Visible = true;
                HealingPanel.Visible = true;
            }
            else
            {
                HealingPanel.Visible = false;
                if(!Wizard.Checked)
                {
                    MagicPanel.Visible = false;
                }
            }
        }

        private void Wizard_CheckedChanged(object sender, EventArgs e)
        {
            if (Wizard.Checked)
            {
                MagicPanel.Visible = true;
                MagicAttackPanel.Visible = true;
            }
            else
            {
                MagicAttackPanel.Visible = false;
                if (!Healer.Checked)
                {
                    MagicPanel.Visible = false;
                }
            }
        }
    }
}