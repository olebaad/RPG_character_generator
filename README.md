# RPG_Character_Generator
This is a Windows Form Project built using [.NET FrameWork](https://docs.microsoft.com/en-us/dotnet/framework/) 4.8.

## Details
The application utilizes a DLL containing a RPG character class library.
The UI allows the user to create characters of different types with abilities, and save the  character details to a textfile.

## Characters
The user can choose between 4 types:

- **Warrior**
Strong type made for battle.

- **Thief**
Weaker type made for sneaking and stealing.

- **Wizard**
Magical type made for war.

- **Healer**
Magical type made for healing.

## Author
Ole Baadshaug